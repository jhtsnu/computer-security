#include <iostream>
#include <string>
#include <map>
#include <algorithm>
#include <stdlib.h>
#include <pcap.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>

using namespace std;

int main(int argc, char *argv[])
{
  char err_buf[PCAP_ERRBUF_SIZE];
  // 프로세스 인자로 파일명을 입력해 준다.
  pcap_t *pcap = pcap_open_offline(argv[1], err_buf);

  const unsigned char* pkt = NULL;
  struct pcap_pkthdr pkthdr;

  struct ip* ip_hdr = NULL;
  struct tcphdr* tcp_hdr = NULL;

  string ip_src, ip_dst;
  // < Source IP, Destination IP > 에 대한 SYN 의 갯수
  map<pair<string,string>,int> syn_map;
  // < Source IP, Destination IP > 에 대한 ACK 의 갯수
  map<pair<string,string>,int> ack_map;

  int total_count = 0; 
  while ((pkt = pcap_next( pcap, &pkthdr ) ) != NULL) {
    //Get IP Header
    ip_hdr = (struct ip*)(pkt + 14);
    //Get TCP Header
    tcp_hdr = (struct tcphdr*)(pkt + 14 + ip_hdr->ip_hl * 4);
    //Parse src, dst to string
    ip_src = inet_ntoa(ip_hdr->ip_src);
    ip_dst = inet_ntoa(ip_hdr->ip_dst);
    
    //Count which is SYN or ACK
    switch (tcp_hdr->th_flags) {
    case TH_SYN:
      syn_map[pair<string, string>(ip_src, ip_dst)]++;
      //for debugging
      //cout << "src : " << ip_src << ", dst : " << ip_dst << " is syn." << endl;
      break;
    case TH_ACK:
      ack_map[pair<string, string>(ip_src, ip_dst)]++;
      //for debugging
      //cout << "src : " << ip_src << ", dst : " << ip_dst << " is ack." << endl;
      break;
    }
    total_count++;
  }
  //for debugging
  //cout << "total : " << total_count << endl;
  map<pair<string,string>,int>::iterator iter;
  int syn_count, ack_count;
  //SYN 의 갯수가 ACK보다 많을 경우를 체크하게 되므로 syn_map을 iterate시키는 것이 합리적이다.
  for (iter = syn_map.begin() ; iter != syn_map.end() ; ++iter) {
    syn_count = iter->second;
    ack_count = ack_map[iter->first];
    //for debugging
    //cout << "syn : " << syn_count << ", ack : " << ack_count << endl;
    if ( syn_count > ack_count * 3 )
      cout << "< " << iter->first.first << ", " << iter->first.second << " >" << endl;
  }

  return 0;
}
