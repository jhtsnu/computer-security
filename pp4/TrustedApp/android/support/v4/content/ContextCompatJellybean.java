package android.support.v4.content;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

class ContextCompatJellybean
{
  public static void startActivities(Context paramContext, Intent[] paramArrayOfIntent, Bundle paramBundle)
  {
    paramContext.startActivities(paramArrayOfIntent, paramBundle);
  }
}

/* Location:           /Volumes/Relppob/Downloads/dex2jar-0.0.9.15/com.cs155.trustedapp-1_dex2jar.jar
 * Qualified Name:     android.support.v4.content.ContextCompatJellybean
 * JD-Core Version:    0.6.0
 */