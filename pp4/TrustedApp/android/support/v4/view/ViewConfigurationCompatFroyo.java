package android.support.v4.view;

import android.view.ViewConfiguration;

class ViewConfigurationCompatFroyo
{
  public static int getScaledPagingTouchSlop(ViewConfiguration paramViewConfiguration)
  {
    return paramViewConfiguration.getScaledPagingTouchSlop();
  }
}

/* Location:           /Volumes/Relppob/Downloads/dex2jar-0.0.9.15/com.cs155.trustedapp-1_dex2jar.jar
 * Qualified Name:     android.support.v4.view.ViewConfigurationCompatFroyo
 * JD-Core Version:    0.6.0
 */