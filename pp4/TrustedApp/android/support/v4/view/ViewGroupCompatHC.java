package android.support.v4.view;

import android.view.ViewGroup;

class ViewGroupCompatHC
{
  public static void setMotionEventSplittingEnabled(ViewGroup paramViewGroup, boolean paramBoolean)
  {
    paramViewGroup.setMotionEventSplittingEnabled(paramBoolean);
  }
}

/* Location:           /Volumes/Relppob/Downloads/dex2jar-0.0.9.15/com.cs155.trustedapp-1_dex2jar.jar
 * Qualified Name:     android.support.v4.view.ViewGroupCompatHC
 * JD-Core Version:    0.6.0
 */