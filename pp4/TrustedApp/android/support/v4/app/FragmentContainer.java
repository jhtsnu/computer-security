package android.support.v4.app;

import android.view.View;

abstract interface FragmentContainer
{
  public abstract View findViewById(int paramInt);
}

/* Location:           /Volumes/Relppob/Downloads/dex2jar-0.0.9.15/com.cs155.trustedapp-1_dex2jar.jar
 * Qualified Name:     android.support.v4.app.FragmentContainer
 * JD-Core Version:    0.6.0
 */