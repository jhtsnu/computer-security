package android.support.v4.app;

import android.util.AndroidRuntimeException;

final class SuperNotCalledException extends AndroidRuntimeException
{
  public SuperNotCalledException(String paramString)
  {
    super(paramString);
  }
}

/* Location:           /Volumes/Relppob/Downloads/dex2jar-0.0.9.15/com.cs155.trustedapp-1_dex2jar.jar
 * Qualified Name:     android.support.v4.app.SuperNotCalledException
 * JD-Core Version:    0.6.0
 */