package com.cs155.trustedapp;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

public class MyResultReceiver extends ResultReceiver
{
  private Receiver mReceiver;

  public MyResultReceiver(Handler paramHandler)
  {
    super(paramHandler);
  }

  protected void onReceiveResult(int paramInt, Bundle paramBundle)
  {
    if (this.mReceiver != null)
      this.mReceiver.onReceiveResult(paramInt, paramBundle);
  }

  public void setReceiver(Receiver paramReceiver)
  {
    this.mReceiver = paramReceiver;
  }

  public static abstract interface Receiver
  {
    public abstract void onReceiveResult(int paramInt, Bundle paramBundle);
  }
}

/* Location:           /Volumes/Relppob/Downloads/dex2jar-0.0.9.15/com.cs155.trustedapp-1_dex2jar.jar
 * Qualified Name:     com.cs155.trustedapp.MyResultReceiver
 * JD-Core Version:    0.6.0
 */