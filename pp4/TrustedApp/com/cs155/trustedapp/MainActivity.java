package com.cs155.trustedapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity
  implements MyResultReceiver.Receiver
{
  public MyResultReceiver mReceiver;

  public void onActivityResult(int paramInt)
  {
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130903040);
    Button localButton = (Button)findViewById(2131230721);
    this.mReceiver = new MyResultReceiver(new Handler());
    this.mReceiver.setReceiver(this);
    localButton.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramView)
      {
        Intent localIntent = new Intent();
        localIntent.setClass(MainActivity.this.getApplicationContext(), ReadContactsService.class);
        localIntent.putExtra("SECRET_KEY", "youllnevergetmeluckycharms");
        localIntent.putExtra("REQUEST_CONTACTS_STRING", MainActivity.this.mReceiver);
        MainActivity.this.startService(localIntent);
      }
    });
  }

  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    getMenuInflater().inflate(2131165184, paramMenu);
    return true;
  }

  public void onReceiveResult(int paramInt, Bundle paramBundle)
  {
    TextView localTextView;
    String str;
    if (paramInt == 1)
    {
      localTextView = (TextView)findViewById(2131230722);
      str = paramBundle.getString("CONTACTS_STRING_KEY");
      if (str == "")
        localTextView.setText("Blank string returned!");
    }
    else
    {
      return;
    }
    localTextView.setText("Contacts:\n" + str);
  }
}

/* Location:           /Volumes/Relppob/Downloads/dex2jar-0.0.9.15/com.cs155.trustedapp-1_dex2jar.jar
 * Qualified Name:     com.cs155.trustedapp.MainActivity
 * JD-Core Version:    0.6.0
 */